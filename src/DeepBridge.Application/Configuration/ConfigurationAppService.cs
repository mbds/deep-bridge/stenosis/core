﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using DeepBridge.Configuration.Dto;

namespace DeepBridge.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : DeepBridgeAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
