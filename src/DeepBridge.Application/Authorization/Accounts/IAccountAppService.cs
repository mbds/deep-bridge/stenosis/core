﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DeepBridge.Authorization.Accounts.Dto;

namespace DeepBridge.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
