﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using DeepBridge.Prediction.Model.Dto;
using Microsoft.AspNetCore.Mvc;

namespace DeepBridge.Prediction.Model
{
    [Route("api/v1/Model")]
    public class ModelAppService : DeepBridgeAsyncCrudAppServiceBase<Predictions.Model, ModelDto, ModelForListDto, ModelForSelectDto, int, EntityDto, GetAllModelRequestDto, ModelDto, ModelDto, EntityDto>, IModelAppService
    {
        public ModelAppService(IRepository<Predictions.Model, int> repository) : base(repository)
        {
        }
    }
}
