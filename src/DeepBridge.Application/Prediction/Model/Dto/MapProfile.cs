﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.Model.Dto
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            //Display oriented mappings
            CreateMap<Predictions.Model, ModelDto>();
            CreateMap<Predictions.Model, ModelForListDto>();
            CreateMap<Predictions.Model, ModelForSelectDto>();


            //Edit oriented mappings
            //TIP Never map child collections, it should be done manually to decide wich are updated, deleted or created
            CreateMap<ModelDto, Predictions.Model>();
            CreateMap<ModelForListDto, Predictions.Model>();
            CreateMap<ModelForSelectDto, Predictions.Model>();
        }
    }
}
