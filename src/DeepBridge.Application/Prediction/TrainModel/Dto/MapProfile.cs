﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.TrainModel.Dto
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            //Display oriented mappings
            CreateMap<Predictions.TrainModel, TrainModelDto>();
            CreateMap<Predictions.TrainModel, TrainModelForListDto>();
            CreateMap<Predictions.TrainModel, TrainModelForSelectDto>();


            //Edit oriented mappings
            //TIP Never map child collections, it should be done manually to decide wich are updated, deleted or created
            CreateMap<TrainModelDto, Predictions.TrainModel>();
            CreateMap<TrainModelForListDto, Predictions.TrainModel>();
            CreateMap<TrainModelForSelectDto, Predictions.TrainModel>();
        }
    }
}
