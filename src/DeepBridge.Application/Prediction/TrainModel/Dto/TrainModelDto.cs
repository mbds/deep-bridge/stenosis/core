﻿using Abp.Application.Services.Dto;

namespace DeepBridge.Prediction.TrainModel.Dto
{
    public class TrainModelDto : EntityDto<int>
    {
        public string Link { get; set; }
        public string Libelle { get; set; }
        public int CasEtudeId { get; set; }
    }
}