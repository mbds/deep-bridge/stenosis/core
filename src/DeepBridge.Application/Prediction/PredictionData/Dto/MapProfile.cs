﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.PredictionData.Dto
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            //Display oriented mappings
            CreateMap<Predictions.PredictionData, PredictionDataDto>();
            CreateMap<Predictions.PredictionData, PredictionDataForListDto>();
            CreateMap<Predictions.PredictionData, PredictionDataForSelectDto>();


            //Edit oriented mappings
            //TIP Never map child collections, it should be done manually to decide wich are updated, deleted or created
            CreateMap<PredictionDataDto, Predictions.PredictionData>();
            CreateMap<PredictionDataForListDto, Predictions.PredictionData>();
            CreateMap<PredictionDataForSelectDto, Predictions.Model>();
        }
    }
}
