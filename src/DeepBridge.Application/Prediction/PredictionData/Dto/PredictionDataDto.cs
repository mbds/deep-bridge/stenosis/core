﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.PredictionData.Dto
{
    public class PredictionDataDto : EntityDto<int>
    {
        public string Libelle { get; set; }
        public string Link { get; set; }
        public int CasEtudeId { get; set; }
        public int PredictionId { get; set; }
        public int UtilisateurId { get; set; }
    }
}
