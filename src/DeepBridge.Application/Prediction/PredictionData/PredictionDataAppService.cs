﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using DeepBridge.Prediction.PredictionData.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.PredictionData
{
    [Route("api/v1/PredictionData")]
    public class PredictionDataAppService : DeepBridgeAsyncCrudAppServiceBase<Predictions.PredictionData, PredictionDataDto, PredictionDataForListDto, PredictionDataForSelectDto, int, EntityDto, GetAllPredictionDataRequestDto, PredictionDataDto, PredictionDataDto, EntityDto>, IPredictionDataAppService
    {
        public PredictionDataAppService(IRepository<Predictions.PredictionData, int> repository) : base(repository)
        {
        }
    }
}
