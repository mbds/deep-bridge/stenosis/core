﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using DeepBridge.Prediction.Prediction.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.Prediction
{
    [Route("api/v1/Prediction")]
    public class PredictionAppService : DeepBridgeAsyncCrudAppServiceBase<Predictions.Prediction, PredictionDto, PredictionForListDto, PredictionForSelectDto, int, EntityDto, GetAllPredictionRequestDto, PredictionDto, PredictionDto, EntityDto>, IPredictionAppService
    {
        public PredictionAppService(IRepository<Predictions.Prediction, int> repository) : base(repository)
        {
        }
    }
}
