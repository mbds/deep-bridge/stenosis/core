﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.Prediction.Dto
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            //Display oriented mappings
            CreateMap<Predictions.Prediction, PredictionDto>();
            CreateMap<Predictions.Prediction, PredictionForListDto>();
            CreateMap<Predictions.Prediction, PredictionForSelectDto>();


            //Edit oriented mappings
            //TIP Never map child collections, it should be done manually to decide wich are updated, deleted or created
            CreateMap<PredictionDto, Predictions.Prediction>();
            CreateMap<PredictionForListDto, Predictions.Prediction>();
            CreateMap<PredictionForSelectDto, Predictions.Prediction>();
        }
    }
}
