﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DeepBridge.Prediction.Prediction.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.Prediction
{
    public interface IPredictionAppService : IApplicationService, IDeepBridgeAsyncCrudAppServiceBase<PredictionDto, PredictionForListDto, PredictionForSelectDto, int, EntityDto, GetAllPredictionRequestDto, PredictionDto, PredictionDto, EntityDto>
    {
    }
}
