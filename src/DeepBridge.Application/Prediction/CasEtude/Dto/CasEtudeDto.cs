﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.Dto
{
    public class CasEtudeDto : EntityDto<int>
    {
        public string LibelleCas { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public int TrainModelId { get; set; }
        public int ModelId { get; set; }
        public int PredictionDataId { get; set; }
        public int AdminId { get; set; }
    }
}
