﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DeepBridge.Prediction.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeepBridge.Prediction.CasEtude
{
    public interface ICasEtudeAppService : IApplicationService, IDeepBridgeAsyncCrudAppServiceBase<CasEtudeDto, CasEtudeForListDto, CasEtudeForSelectDto, int, EntityDto, GetAllCasEtudeRequestDto, CasEtudeDto, CasEtudeDto, EntityDto>
    {
    }
}
