﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DeepBridge
{
    public interface IDeepBridgeAsyncCrudAppServiceBase<TEntityDto, TListEntityDto, TSelectEntityDto, TPrimaryKey, TGetInput, TGetAllInput, TCreateInput, TUpdateInput, TDeleteInput>
        where TEntityDto : IEntityDto<TPrimaryKey>
        where TGetInput : IEntityDto<TPrimaryKey>
        where TUpdateInput : IEntityDto<TPrimaryKey>
        where TDeleteInput : IEntityDto<TPrimaryKey>
    {
        Task<ActionResult<TEntityDto>> Create(TCreateInput input);
        Task DeleteAsync(TDeleteInput input);
        Task<TEntityDto> Get(TGetInput input);
        Task<PagedResultDto<TEntityDto>> GetAll(TGetAllInput input);
        Task<PagedResultDto<TListEntityDto>> GetAllForList(TGetAllInput input);
        Task<PagedResultDto<TSelectEntityDto>> GetAllForSelect(TGetAllInput input);
        Task<ActionResult<TEntityDto>> Update(TUpdateInput input);
    }
}