﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeepBridge.Migrations
{
    public partial class CreationDesEntitesDeepBridge : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Prediction",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Link = table.Column<string>(maxLength: 255, nullable: true),
                    Type = table.Column<string>(maxLength: 100, nullable: false),
                    PredictionDataId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prediction", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TypeClassification",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeClassification", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TypeData",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeData", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TypeSegmentation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeSegmentation", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Utilisateur",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Login = table.Column<string>(maxLength: 100, nullable: false),
                    CryptedMdp = table.Column<string>(maxLength: 255, nullable: false),
                    Nom = table.Column<string>(maxLength: 100, nullable: false),
                    Prenom = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Utilisateur", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Admin",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(maxLength: 100, nullable: false),
                    UtilisateurId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Admin", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Admin__Utilisateur",
                        column: x => x.UtilisateurId,
                        principalTable: "Utilisateur",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Doctor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Matricule = table.Column<string>(maxLength: 255, nullable: false),
                    UtilisateurId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Doctor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Doctor__Utilisateur",
                        column: x => x.UtilisateurId,
                        principalTable: "Utilisateur",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CasEtude",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LibelleCas = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Link = table.Column<string>(maxLength: 255, nullable: true),
                    TrainModelId = table.Column<int>(nullable: false),
                    ModelId = table.Column<int>(nullable: false),
                    PredictionDataId = table.Column<int>(nullable: false),
                    AdminId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CasEtude", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CasEtude__Admin",
                        column: x => x.AdminId,
                        principalTable: "Admin",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Model",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Link = table.Column<string>(maxLength: 255, nullable: true),
                    Task = table.Column<string>(maxLength: 100, nullable: true),
                    ImResize = table.Column<int>(nullable: false),
                    CasEtudeId = table.Column<int>(nullable: false),
                    Aug = table.Column<bool>(nullable: false),
                    Normalize = table.Column<bool>(nullable: false),
                    TypeDataId = table.Column<int>(nullable: false),
                    TypeSegmentationId = table.Column<int>(nullable: false),
                    TypeClassificationId = table.Column<int>(nullable: false),
                    Batch = table.Column<int>(nullable: false),
                    Epoch = table.Column<int>(nullable: false),
                    Rate = table.Column<int>(nullable: false),
                    Optimizeur = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Model", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Model__CasEtude",
                        column: x => x.CasEtudeId,
                        principalTable: "CasEtude",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Model__TypeClassification",
                        column: x => x.TypeClassificationId,
                        principalTable: "TypeClassification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Model__TypeData",
                        column: x => x.TypeDataId,
                        principalTable: "TypeData",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Model__TypeSegmentation",
                        column: x => x.TypeSegmentationId,
                        principalTable: "TypeSegmentation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PredictionData",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Libelle = table.Column<string>(maxLength: 100, nullable: false),
                    Link = table.Column<string>(maxLength: 255, nullable: true),
                    CasEtudeId = table.Column<int>(nullable: false),
                    PredictionId = table.Column<int>(nullable: false),
                    UtilisateurId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PredictionData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PredictionData__CasEtude",
                        column: x => x.CasEtudeId,
                        principalTable: "CasEtude",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PredictionData__Prediction",
                        column: x => x.PredictionId,
                        principalTable: "Prediction",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PredictionData__Utilisateur",
                        column: x => x.UtilisateurId,
                        principalTable: "Utilisateur",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TrainModel",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Link = table.Column<string>(maxLength: 255, nullable: true),
                    Libelle = table.Column<string>(maxLength: 100, nullable: false),
                    CasEtudeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainModel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TrainModel__CasEtude",
                        column: x => x.CasEtudeId,
                        principalTable: "CasEtude",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Admin_UtilisateurId",
                table: "Admin",
                column: "UtilisateurId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CasEtude_AdminId",
                table: "CasEtude",
                column: "AdminId");

            migrationBuilder.CreateIndex(
                name: "IX_Doctor_UtilisateurId",
                table: "Doctor",
                column: "UtilisateurId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Model_CasEtudeId",
                table: "Model",
                column: "CasEtudeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Model_TypeClassificationId",
                table: "Model",
                column: "TypeClassificationId");

            migrationBuilder.CreateIndex(
                name: "IX_Model_TypeDataId",
                table: "Model",
                column: "TypeDataId");

            migrationBuilder.CreateIndex(
                name: "IX_Model_TypeSegmentationId",
                table: "Model",
                column: "TypeSegmentationId");

            migrationBuilder.CreateIndex(
                name: "IX_PredictionData_CasEtudeId",
                table: "PredictionData",
                column: "CasEtudeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PredictionData_PredictionId",
                table: "PredictionData",
                column: "PredictionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PredictionData_UtilisateurId",
                table: "PredictionData",
                column: "UtilisateurId");

            migrationBuilder.CreateIndex(
                name: "IX_TrainModel_CasEtudeId",
                table: "TrainModel",
                column: "CasEtudeId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Doctor");

            migrationBuilder.DropTable(
                name: "Model");

            migrationBuilder.DropTable(
                name: "PredictionData");

            migrationBuilder.DropTable(
                name: "TrainModel");

            migrationBuilder.DropTable(
                name: "TypeClassification");

            migrationBuilder.DropTable(
                name: "TypeData");

            migrationBuilder.DropTable(
                name: "TypeSegmentation");

            migrationBuilder.DropTable(
                name: "Prediction");

            migrationBuilder.DropTable(
                name: "CasEtude");

            migrationBuilder.DropTable(
                name: "Admin");

            migrationBuilder.DropTable(
                name: "Utilisateur");
        }
    }
}
