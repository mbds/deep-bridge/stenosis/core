﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using DeepBridge.Configuration;
using DeepBridge.Web;

namespace DeepBridge.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class DeepBridgeDbContextFactory : IDesignTimeDbContextFactory<DeepBridgeDbContext>
    {
        public DeepBridgeDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DeepBridgeDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            DeepBridgeDbContextConfigurer.Configure(builder, configuration.GetConnectionString(DeepBridgeConsts.ConnectionStringName));

            return new DeepBridgeDbContext(builder.Options);
        }
    }
}
