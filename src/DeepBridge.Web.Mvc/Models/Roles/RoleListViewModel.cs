﻿using System.Collections.Generic;
using DeepBridge.Roles.Dto;

namespace DeepBridge.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
