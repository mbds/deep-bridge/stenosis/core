﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace DeepBridge.Localization
{
    public static class DeepBridgeLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(DeepBridgeConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(DeepBridgeLocalizationConfigurer).GetAssembly(),
                        "DeepBridge.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
