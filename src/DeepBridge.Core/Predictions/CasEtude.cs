﻿using Abp.Domain.Entities;
using DeepBridge.Personnes;

namespace DeepBridge.Predictions
{
    public class CasEtude : Entity<int>
    {
        public string LibelleCas { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public int TrainModelId { get; set; }
        public int ModelId { get; set; }
        public int PredictionDataId { get; set; }
        public int AdminId { get; set; }

        // Proprietée de navigation
        public virtual TrainModel TrainModel { get; set; }
        public virtual Model Model { get; set; }
        public virtual PredictionData PredictionData { get; set; }
        public virtual Admin Admin { get; set; }
    }
}
