﻿using Abp.Domain.Entities;
using DeepBridge.Segments;

namespace DeepBridge.Predictions
{
    public class Model : Entity<int>
    {
        public string Link { get; set; }
        public string Task { get; set; }
        public int ImResize { get; set; }
        public int CasEtudeId { get; set; }
        public bool Aug { get; set; }
        public bool Normalize { get; set; }
        public int TypeDataId { get; set; }
        public int TypeSegmentationId { get; set; }
        public int TypeClassificationId { get; set; }
        public int Batch { get; set; }
        public int Epoch { get; set; }
        public int Rate { get; set; }
        public string Optimizeur { get; set; }

        // Proprietée de navigation
        public virtual CasEtude CasEtude { get; set; }
        public virtual TypeData TypeData { get; set; }
        public virtual TypeSegmentation TypeSegmentation { get; set; }
        public virtual TypeClassification TypeClassification { get; set; }
    }
}
